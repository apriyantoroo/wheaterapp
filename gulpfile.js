// Initialize package
var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var browserSync = require('browser-sync');

// Register sources and destination files
var sourceSass = './resource/sass/**/*.scss';
var destSass = './public/css/';
var sourceJs = './resource/js/*.js';
var destJs = './public/js/';
var sourceHTML = './resource/html/*.pug';
var destHTML = './public/';
var sourcesImg = './resource/img/*.{jpg,png,gif,svg}';
var destImg = './public/img';

// Compile template engine to HTML files
gulp.task('pug', function() {
    return gulp.src(sourceHTML)
      .pipe(pug({
      	pretty: true
      }))
      .pipe(gulp.dest(destHTML));
});

// Compile CSS prepocessor to css files
gulp.task('sass', function() {
    return gulp.src(sourceSass)
      .pipe(sourcemaps.init({
      	loadMaps: true
      }))
      .pipe(sass())
      .pipe(autoprefixer())
      .pipe(sourcemaps.write('/'))
      .pipe(gulp.dest(destSass));
});

// Initialize local server
gulp.task('serve', function() {
    browserSync.init({
    	server: {
    		baseDir: './public'
    	}
    })

    gulp.watch(sourceHTML, ['pug']);
    gulp.watch(sourceSass, ['sass']);
    gulp.watch(sourceJs, ['js']);
    gulp.watch(sourcesImg, ['imagemin']);
    gulp.watch(destHTML).on('change', browserSync.reload);
    gulp.watch(destSass).on('change', browserSync.reload);
    gulp.watch(destJs).on('change', browserSync.reload);
    gulp.watch(destImg).on('change', browserSync.reload);
});

gulp.task('default', ['pug', 'js', 'sass', 'imagemin', 'browserSync', 'watch']);